import os
import numpy as np

from keras.utils import to_categorical
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.callbacks import TensorBoard, ModelCheckpoint
from keras.layers import Embedding, Conv1D, MaxPooling1D, Flatten, Dense, SpatialDropout1D
from keras.engine import Input
from keras.models import Model, load_model

from gensim.models import KeyedVectors

class CNNModel(object):
    def __init__(self, train_filename,
                       dev_filename,
                       german_word_embeddings_filename):
        """ Constructor for our CNN model with pretrained word embeddings

        This method parses the training and development set

        # Arguments
          train_filename                 : The training set
          dev_filename                   : The development set
          german_word_embeddings_filename: The pretrained word embeddings for
                                           German. Notice: we really expect the
                                           model file (german.model)from:
                                           http://devmount.github.io/GermanWordEmbeddings/
        """
        self.train_filename = train_filename
        self.dev_filename = dev_filename
        self.german_word_embeddings_filename = german_word_embeddings_filename

        # Lowercasing will loose ~ 7 percent on accuracy
        self.tokenizer = Tokenizer(lower=False)
        self.word_index = {}
        self.__parse_datasets__()

    def __parse_dataset__(self, filename):
        """ Parses a dataset

        More precisely we expect the following data format:

        0	Er fühlte keine Kälte, auch war er immerfort in Bewegung.

        As it can be seen in the example above, the data set is tabulator
        separated. The first field contains the label, the second field contains
        the sentence. More details can be found here:https://gitlab.com/kafka-mann/dataset

        The data set is then splitted into a sentences array (named X) and a
        numpy array containing all the labels (named y).

        Notice: In the sentence array all non-alpha characters, like punctuation
        chars, are removed.

        # Arguments
          filename: The filename of the data set to be parsed

        # Returns
          X: The parsed sentences array
          y: The numpy array of all labels
        """
        with open(filename) as f:
            lines = [line for line in f.readlines()]

            X = [''.join([i for i in line.split("\t")[1] if i.isalpha() or i == " "]).strip() for line in lines]

            y = np.asarray([int(line.split("\t")[0]) for line in lines])

            return X, y

    def __parse_datasets__(self):
        """ Parses all data sets

        This method parses training and development set. After that, the
        sentence array is tokenized. After the tokenization process the
        sentence array is going to be converted into words indices. That means
        every word in the sentence get an id/index number. Then the sentence
        array will be padded to a length of 1000.

        The label numpy array is going to be converted to a one-hot vector.

        These steps are done both for the training and development set.

        As a final result we got a training set (named X_train) and a
        corresponding label array (named as y_train), a development set
        (named X_dev) and a corresponding label array (named y_dev). These four
        objects are going to be saved as member variables here.
        """

        X_train, y_train = self.__parse_dataset__(self.train_filename)
        X_dev, y_dev = self.__parse_dataset__(self.dev_filename)

        self.tokenizer.fit_on_texts(X_train + X_dev)

        X_train = self.tokenizer.texts_to_sequences(X_train)
        X_train = pad_sequences(X_train, maxlen=1000, padding='post')
        y_train = to_categorical(y_train)

        X_dev = self.tokenizer.texts_to_sequences(X_dev)
        X_dev = pad_sequences(X_dev, maxlen=1000, padding='post')
        y_dev = to_categorical(y_dev)

        self.X_train = X_train
        self.y_train = y_train
        self.X_dev = X_dev
        self.y_dev = y_dev

    def __construct_embedding_layer__(self):
        """ Constructs an embedding layer

        These method reads pretrained German word embeddings with a fixed
        dimension of 300.

        Then an embedding matrix for the tokenized words (which appeared both
        in training and development set) is constructed.

        In the final step, a Keras Embedding Layer is built. The embedding
        matrix is used for weight initialization in the Keras Embedding layer.
        """

        word2vec = KeyedVectors.load_word2vec_format(self.german_word_embeddings_filename, binary=True)

        self.word_index = self.tokenizer.word_index

        embedding_matrix = np.zeros((len(self.word_index) + 1, 300))
        for word, i in self.word_index.items():
            if word in word2vec.vocab:
                embedding_matrix[i] = word2vec.word_vec(word)

        sequence_input = Input(shape=(1000,), dtype='int32')

        embedding_layer = Embedding(len(self.word_index) + 1,
                                    300,
                                    weights=[embedding_matrix],
                                    input_length=1000,
                                    name="embedding_layer",
                                    trainable=False)

        self.embedding_layer = embedding_layer

    def __construct_model__(self):
        """ Constructs a Keras model

        In this method we construct a stacked CNN for text classification.
        """
        sequence_input = Input(shape=(1000,), dtype='int32')
        embedded_sequences = self.embedding_layer(sequence_input)
        x = Conv1D(128, 5, activation='relu')(embedded_sequences)
        x = MaxPooling1D(5)(x)
        x = Conv1D(128, 5, activation='relu')(x)
        x = MaxPooling1D(5)(x)
        x = Conv1D(128, 5, activation='relu')(x)
        x = MaxPooling1D(35)(x)
        x = Flatten()(x)
        x = Dense(128, activation='relu')(x)

        preds = Dense(2, activation='sigmoid')(x)

        model = Model(sequence_input, preds)

        self.model = model

    def __construct_tensorboard_callback__(self, tensorboard_log_dir):
        """ Constructs a callback function for TensorBoard

        In this method we initialize a Keras callback function to use Keras
        with the TensorFlow "TensorBoard" to show some nice visualizations.

        During or after training simply run:

        tensorboard --logdir="./log"

        and point your browser to http://localhost:6006.

        More information about TensorBoard can be found here:

        https://www.tensorflow.org/get_started/summaries_and_tensorboard
        """

        # Write metadata file to get a nice mapping between indices and words
        embeddings_metadata = ["Metadata"] + \
                              ["{}\t{}".format(item[0], item[1])
                               for item in self.word_index.items()]

        if not os.path.exists(tensorboard_log_dir):
            os.makedirs(tensorboard_log_dir)

        with open(tensorboard_log_dir + "/embeddings_metadata.tsv", "w") as f:
            f.write("\n".join(embeddings_metadata))

        self.tensorboard_callback = TensorBoard(log_dir=tensorboard_log_dir,
                                                histogram_freq=1,
                                                write_graph=True,
                                                write_grads=False,
                                                embeddings_freq=1,
                                                embeddings_layer_names=["embedding_layer"],
                                                embeddings_metadata={"embedding_layer": "embeddings_metadata.tsv"},
                                                write_images=True)

    def __construct_best_model_callback(self, best_model_filename):
        """ Constructs a Keras Callback to save the best model

        This method builds a Keras Callback to save the best model. Saving the
        best model will be done after each training epoch. Here we make sure,
        that only the best model according to the accuracy on the development
        set is saved. The final evaluation method loads the best model and
        does evaluation on it.

        # Arguments
          best_model_filename: The filename for the to be saved best model

        """
        self.best_model_callback = ModelCheckpoint(best_model_filename,
                                                   monitor="val_acc",
                                                   save_best_only=True,
                                                   save_weights_only=False,
                                                   mode='max')

    def train(self, batch_size=128,
                    epochs=15,
                    best_model_filename="best_model.hdf5",
                    optimizer='rmsprop',
                    tensorboard_log_dir="./log"):
        """ Runs main training

        This method is responsible for the whole training process:

        - Construct the embedding layer
        - Build the Keras model
        - Initialize callbacks for TensorBoard and saving the best model

        After these steps, the Keras model will be compiled and the training
        starts.

        # Arguments
          batch_size         : The batch size used for training
          epochs             : The number of epochs
          best_model_filename: The filename for the best model
          optimizer          : The to be used optimizer
          tensorboard_log_dir: The log directory for TensorBoard
        """
        self.__construct_embedding_layer__()
        self.__construct_model__()

        self.__construct_tensorboard_callback__(tensorboard_log_dir)
        self.__construct_best_model_callback(best_model_filename)

        self.model.compile(loss='categorical_crossentropy',
                           optimizer=optimizer,
                           metrics=['acc'])

        self.model.fit(self.X_train, self.y_train,
                       batch_size=batch_size,
                       epochs=epochs,
                       validation_data=(self.X_dev, self.y_dev),
                       callbacks=[self.tensorboard_callback,
                                  self.best_model_callback])

    def test(self, batch_size=128,
                   best_model_filename="best_model.hdf5"):
        """  Runs main evaluation

        This method runs evaluation on the development set. It loads the
        previously saved best model (training) and starts evaluation.

        # Arguments
          batch_size         : The batch size used for evaluation
          best_model_filename: The filename for the best model
        """
        self.best_model = load_model(best_model_filename)

        scores = self.best_model.evaluate(self.X_dev,
                                          self.y_dev,
                                          batch_size=batch_size)
        print("\n%s: %.4f%%" % (self.best_model.metrics_names[1], scores[1]))
