# CNN + Pretrained Word Embeddings for Kafka-Mann Classification Task

This repository contains all code to solve the
[Kafka-Mann Classification Task](https://gitlab.com/kafka-mann/task) with
stacked convolutional neural networks (CNNs) and pretrained German word
embeddings.

# Dependencies

Here's a small matrix for tested versions:

| Library      | Tested version
| ------------ | -------------------
| *Python*     | 3.6.1
| *Keras*      | 2.0.8
| *Tensorflow* | 1.3.0

As *Theao* is [dead](https://groups.google.com/forum/#!topic/theano-users/7Poq8BZutbY)
and this code uses *TensorBoard* for training visualizations, we choose
*TensorFlow* as *Keras* backend.

# Dataset

Training and development set for the Kafka-Mann Classification Task can be
found [here](https://gitlab.com/kafka-mann/dataset). Simply clone the
repository with:

```bash
https://gitlab.com/kafka-mann/dataset.git
```

# German Word Embeddings

Our implementation uses pretrained German word embeddings from [here](http://devmount.github.io/GermanWordEmbeddings/).

The dimension of the word embeddings is 300. Just download the `german.model`:

```bash
wget https://tubcloud.tu-berlin.de/s/dc4f9d207bcaf4d4fae99ab3fbb1af16/download
```

The file size is 703.8 MB.

# Training

Our model can be trained with the following command:

```bash
python3 nn.py train --training-set train --development-set dev
```

The best model (based on accuracy on the development set) will be saved during
training process. You can use *TensorBoard* for visualizations:

```bash
tensorboard --logdir="./log"
```

Then point your browser to *http://localhost:6006* to see all visualizations.

# Evaluation

After training the model can be evaluated on the development set:

```bash
python3 nn.py test --training-set train --development-set dev
```

# Architecture

We use stacked convolutional neural networks with pretrained word embeddings.
Here is a nice visualization of our used architecture:

![alt text](img/model.png)

# Further commandline options

Here comes the complete list of commandline options:

```bash
$ python3 nn.py --help
Using TensorFlow backend.
usage: nn.py [-h] [--training-set TRAINING_SET]
             [--development-set DEVELOPMENT_SET]
             [--word-embeddings WORD_EMBEDDINGS] [--epochs EPOCHS]
             [--optimizer OPTIMIZER] [--batch-size BATCH_SIZE]
             [--model-filename MODEL_FILENAME]
             [--tensorboard-logdir TENSORBOARD_LOGDIR]
             {train,test}

positional arguments:
  {train,test}

optional arguments:
  -h, --help            show this help message and exit
  --training-set TRAINING_SET
                        Defines training set
  --development-set DEVELOPMENT_SET
                        Defines development set
  --word-embeddings WORD_EMBEDDINGS
                        Defines filename for German Word Embeddings
  --epochs EPOCHS       Defines number of training epochs
  --optimizer OPTIMIZER
                        Defines optimizer used for training
  --batch-size BATCH_SIZE
                        Defines number of batch size
  --model-filename MODEL_FILENAME
                        Defines model filename
  --tensorboard-logdir TENSORBOARD_LOGDIR
                        Defines log directory for TensorBoard
```

# Results

*TensorBoard* is a great tool to visualize the training process. Here's a
snapshot after 15 epochs:

![alt text](img/training-15-epochs.png)

| System | Description                   | Accuracy development set | Model
| ------ | ----------------------------- | ------------------------ | --------------
| 1      | Default parameters, 15 epochs | 0.8439                   | [here](models/best_model-1.hdf5)

# Contact (Bugs, Feedback, Contribution and more)

For questions about the *kafka-mann-cnn-emb* repository, contact the current maintainer:
Stefan Schweter <stefan@schweter.it>. If you want to contribute to the project
please refer to the [Contributing](CONTRIBUTING.md) guide!

# License

To respect the Free Software Movement and the enormous work of Dr. Richard Stallman
the software in this repository is released under the *GNU Affero General Public License*
in version 3. More information can be found [here](https://www.gnu.org/licenses/licenses.html)
and in `COPYING`.
