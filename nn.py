import argparse

from cnnmodel import CNNModel


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("mode", choices=["train", "test"])

    parser.add_argument("--training-set",
                        help="Defines training set")
    parser.add_argument("--development-set",
                        help="Defines development set")
    parser.add_argument("--word-embeddings", default="german.model",
                        help="Defines filename for German Word Embeddings")
    parser.add_argument("--epochs", default=15,
                        help="Defines number of training epochs")
    parser.add_argument("--optimizer", default='rmsprop',
                        help="Defines optimizer used for training")
    parser.add_argument("--batch-size", default=128,
                        help="Defines number of batch size")
    parser.add_argument("--model-filename", default='best_model.hdf5',
                        help="Defines model filename")
    parser.add_argument("--tensorboard-logdir", default='./log',
                        help="Defines log directory for TensorBoard")

    args = parser.parse_args()

    if not args.training_set:
        print("Training set is missing!")
        parser.print_help()
        exit(1)

    if not args.development_set:
        print("Development set is missing!")
        parser.print_help()
        exit(1)

    cnnmodel = CNNModel(args.training_set,
                        args.development_set,
                        args.word_embeddings)

    if args.mode == "train":
        cnnmodel.train(batch_size=int(args.batch_size),
                       epochs=int(args.epochs),
                       best_model_filename=args.model_filename,
                       optimizer=args.optimizer,
                       tensorboard_log_dir=args.tensorboard_logdir)

    elif args.mode == "test":
        cnnmodel.test(batch_size=int(args.batch_size),
                      best_model_filename=args.model_filename)

if __name__ == '__main__':
    parse_arguments()
